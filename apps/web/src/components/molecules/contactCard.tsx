import React from 'react';
import { Box, Typography, Avatar } from '@mui/material';
import { SystemStyleObject, Theme } from '@mui/system';

import { Card, EditableInput } from '@components/atoms';
import { IContact } from 'react-coding-interview-shared/models';

export interface IContactCardProps {
  person: IContact;
  sx?: SystemStyleObject<Theme>;
}

export const ContactCard: React.FC<IContactCardProps> = ({
  person: { name, email },
  sx,
}) => {
  const [cName, setCName] = React.useState(name);
  const [cEmail, setCEmail] = React.useState(email);

  const handleChangeName = (value: string) => {
    setCName(value);
  };

  const handleChangeEmail = (value: string) => {
    setCEmail(value);
  };

  return (
    <Card sx={sx}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar />
        <Box textAlign="center" mt={2}>
          <EditableInput
            initialState={cName}
            variant="subtitle1"
            label="Name"
            lineHeight="1rem"
            handleChange={(e: string) => handleChangeName(e)}
          />
          <EditableInput
            initialState={cEmail}
            variant="caption"
            color="text.secondary"
            label="Name"
            handleChange={(e: string) => handleChangeEmail(e)}
          />
        </Box>
      </Box>
    </Card>
  );
};
