import React from 'react';
import { TextField, Typography, Button } from '@mui/material';

interface EditableInput {
  initialState: string;
  label: string;
  handleChange(e: string): void;
}

export const EditableInput: React.FC<EditableInput> = ({
  initialState,
  label,
  handleChange,
}) => {
  const [value, setValue] = React.useState(initialState);
  const [isEditable, setIsEditable] = React.useState(false);

  return (
    <>
      {isEditable ? (
        <>
          <TextField label={label} value={value} />
          <Button>Save Changes</Button>
        </>
      ) : (
        <>
          <Typography variant="body1" onClick={() => setIsEditable(true)}>
            {value}
          </Typography>
        </>
      )}
    </>
  );
};
